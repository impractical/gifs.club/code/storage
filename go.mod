module gifs.club/code/storage

require (
	cloud.google.com/go v0.27.0
	contrib.go.opencensus.io/exporter/stackdriver v0.6.0 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/google/martian v2.1.0+incompatible // indirect
	github.com/googleapis/gax-go v2.0.0+incompatible // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-memdb v0.0.0-20180223233045-1289e7fffe71
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	gitlab.com/paddycarver/magic-number-checker v0.0.1
	go.opencensus.io v0.15.0 // indirect
	golang.org/x/net v0.0.0-20180906233101-161cd47e91fd // indirect
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be // indirect
	golang.org/x/sync v0.0.0-20180314180146-1d60e4601c6f // indirect
	golang.org/x/sys v0.0.0-20180907202204-917fdcba135d // indirect
	golang.org/x/text v0.3.0 // indirect
	google.golang.org/api v0.0.0-20180907210053-b609d5e6b7ab
	google.golang.org/appengine v1.1.0 // indirect
	google.golang.org/genproto v0.0.0-20180831171423-11092d34479b // indirect
	google.golang.org/grpc v1.14.0 // indirect
	gopkg.in/h2non/filetype.v1 v1.0.5 // indirect
	yall.in v0.0.1
)
